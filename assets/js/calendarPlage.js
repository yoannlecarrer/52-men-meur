/* 
calendar.getMonth() == 0   // JANVIER
calendar.getMonth() == 1   // FEVRIER
calendar.getMonth() == 2   // MARS
calendar.getMonth() == 3   // AVRIL
calendar.getMonth() == 4   // MAI
calendar.getMonth() == 5   // JUIN
calendar.getMonth() == 6   // JUILLET
calendar.getMonth() == 7   // AOUT
calendar.getMonth() == 8   // SEPTEMBRE
calendar.getMonth() == 9   // OCOTOBRE
calendar.getMonth() == 10  // NOVEMBRE
calendar.getMonth() == 11  // DECEMBRE

today.getDate() == date du jour 
today.getMount() == le mois du jour actuel
today.getFullYears == l'année du jour actuel

endate = le nombre de jour dans le mois

calendar.getFullYears == l'année du calendrier

PENSER A TOUJOURS SURPRIMER LES RESERVATION PASSER AU FUR ET A MESURE SINON 
PROBLEME LORS DU CHANGEMENT D'ANNEE

*/
	

let calendar = new Date();

const renderDate = () => {

	let months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
	let day = calendar.getDay();
	let today = new Date();

	calendar.setDate(1);
	let endDate = new Date(
		calendar.getFullYear(),
		calendar.getMonth() + 1,
		0
	).getDate();

	let prevDate = new Date(
		calendar.getFullYear(),
		calendar.getMonth(),
		0
	).getDate();

	document.getElementById("month").innerHTML = months[calendar.getMonth()];
	document.getElementById("date_str").innerHTML = calendar.getFullYear();

	let cells = "";
	for (x = day; x > 0; x--) {
		cells += "<div class='prev_date'>" + (prevDate - x + 1) + "</div>";
	}

	// POUR LES RESERVATONS TOUS SE PASSE ICI
	for (i = 1; i <= endDate; i++) {

		// Si on veux réserver pour l'année actuel
		if (today.getFullYear() == calendar.getFullYear()) {

			/* EXEMPLE :
			ICI ON RESERVE LE 1,2,3 MARS
			if(( i==1 || i==2 || i==3) && calendar.getMonth() == 2 ){
				cells += "<div>" + i + "</div>";
			}
			*/

			if(( i==1 || i==2 || i==3) && calendar.getMonth() == 0 ){
				cells += "<div>" + i + "</div>";
			}

			else if (calendar.getMonth() > today.getMonth()) {
				cells += "<div class='available' >" + i + "</div>";
			}
			else if (i < today.getDate() || calendar.getMonth() < today.getMonth()) {
				cells += "<div>" + i + "</div>";
			}
			else if (i == today.getDate() && calendar.getMonth() == today.getMonth()) {
				cells += "<div class='today'>" + i + "</div>";
			}
			else {
				cells += "<div class='available'>" + i + "</div>";
			}

		// Si on veux réserver pour une année futur
		} else if (today.getFullYear() < calendar.getFullYear()) {

			/* EXEMPLE :
			ICI ON RESERVE LE 1,2,3 Janvier de l'année 2022
			if(( i==1 || i==2 || i==3) && calendar.getMonth() == 0 && calendar.getFullYear() == 2022){
				cells += "<div>" + i + "</div>";
			}
			*/

			if(( i==1 || i==2 || i==3) && calendar.getMonth() == 0 && calendar.getFullYear() == 2021){
				cells += "<div>" + i + "</div>";
			}

			else{
				cells += "<div class='available'>" + i + "</div>";
			}

		} else {
			cells += "<div>" + i + "</div>";
		}
	}
	document.getElementsByClassName("days")[0].innerHTML = cells;

}

// Bouton pour changer de mois 
const moveDate = (para) => {
	if (para == "prev") {
		calendar.setMonth(calendar.getMonth() - 1);
	} else if (para == 'next') {
		calendar.setMonth(calendar.getMonth() + 1);
	}
	renderDate();
}

