let calendar = new Date();

const renderDate = () => {

	let months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
	let day = calendar.getDay();
	let today = new Date();

	calendar.setDate(1);
	let endDate = new Date(
		calendar.getFullYear(),
		calendar.getMonth() + 1,
		0
	).getDate();

	let prevDate = new Date(
		calendar.getFullYear(),
		calendar.getMonth(),
		0
	).getDate();

	document.getElementById("month").innerHTML = months[calendar.getMonth()];
	document.getElementById("date_str").innerHTML = calendar.getFullYear();

	let cells = "";
	for (x = day; x > 0; x--) {
		cells += "<div class='prev_date'>" + (prevDate - x + 1) + "</div>";
	}

	for (i = 1; i <= endDate; i++) {
		if (today.getFullYear() == calendar.getFullYear()) {

			// reservation test

			if( (i == 1 || i==2 || i==3) && calendar.getMonth() == 9 ){
				cells += "<div>" + i + "</div>";
			}

			else if( (i == 21 || i==22 || i==23 || i==24 || i==25 || i==26) && calendar.getMonth() == 8 ){
				cells += "<div>" + i + "</div>";
			}

			// 

			else if (calendar.getMonth() > today.getMonth()) {
				cells += "<div class='available' >" + i + "</div>";
			}
			else if (i < today.getDate() || calendar.getMonth() < today.getMonth()) {
				cells += "<div>" + i + "</div>";
			}
			else if (i == today.getDate() && calendar.getMonth() == today.getMonth()) {
				cells += "<div class='today'>" + i + "</div>";
			}
			else {
				cells += "<div class='available'>" + i + "</div>";
			}

		} else if (today.getFullYear() < calendar.getFullYear()) {
			cells += "<div class='available'>" + i + "</div>";
		} else {
			cells += "<div>" + i + "</div>";
		}
	}
	document.getElementsByClassName("days")[0].innerHTML = cells;

}

const moveDate = (para) => {
	if (para == "prev") {
		calendar.setMonth(calendar.getMonth() - 1);
	} else if (para == 'next') {
		calendar.setMonth(calendar.getMonth() + 1);
	}
	renderDate();
}

